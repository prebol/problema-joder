package problema;

import java.util.Scanner;

public class te_falta_experiencia {

	public static void main(String[] args) {
		//Nivel actual
		//Exp actual
		//Exp subir de nivel (incrementa x 1.2 cada nivel)
		/*Rango de la mazmorra
		 * 
		 * 	S = 70
		 *  A = 50
		 *  B = 30
		 *  C = 20
		 *  D = 10
		 *  E = 5
		 *  
		 */ 
		//Matriz de INTS
		
		
		Scanner sc = new Scanner(System.in);
		
		int nivel = sc.nextInt();
		
	
		int actualXP2 = sc.nextInt();
		double actualXP = actualXP2;
		
		/*para que no sea negativo
		int metaXP2 = 0;
		Boolean flag = false;
		while (!flag) {
			metaXP2 = sc.nextInt();
			if (metaXP2 >= 0) {   //PARA QUE SEA MAS GRANDE QUE ACTUALXP: metaXP2 >= actualXP2
				flag = true;
			}
		}*/
		
		int metaXP2 = sc.nextInt();
		double metaXP = metaXP2;
			
		
		/*-----------------*/
			
		sc.nextLine();
		String rango = sc.nextLine();
		
		int multiplicador = 0;
		
		switch (rango) {
		case "S":
			multiplicador = 70;
			break;
		case "A":
			multiplicador = 50;
			break;
		case "B":
			multiplicador = 30;
			break;
		case "C":
			multiplicador = 20;
			break;
		case "D":
			multiplicador = 10;
			break;
		case "E":
			multiplicador = 5;
			break;
		}
		
			
			/*-----------------------*/
		
		int [][] mazmorra = new int[5][5];

		for (int i = 0;i < 5;i++) {
			for (int j = 0;j < 5;j++) {
				mazmorra[i][j] = sc.nextInt();
			}
		}
		
		
		
		String resultat = resExperiencia(nivel, actualXP, metaXP, multiplicador, mazmorra);
		System.out.println(resultat);
	}

	public static String resExperiencia(int nivel, double actualXP, double metaXP, int multiplicador, int[][] mazmorra) {
		
		//Cada monstre {1, 2, 3, 4, 5, 6, 7, 8, 9} et dona com a EXP el seu valor multiplicat x 'multiplicador'
		//menys els que son divisibles entre 3, aquest et donen el seu valor multiplicat x 'multiplicador' + el seu 25%

		double XPGuanyada = 0;
		
		double bonus = multiplicador * 0.25;
		
		//EXP Total Ganada en la Mazmorra (double)
		for (int i = 0;i < 5;i++) {
			for (int j = 0;j < 5;j++) {
				if (mazmorra[i][j] != 0) {
					if (mazmorra[i][j] % 3 == 0) {
						XPGuanyada = XPGuanyada + (mazmorra[i][j]*(multiplicador+bonus));
					} else {
						XPGuanyada = XPGuanyada + (mazmorra[i][j]*multiplicador);
					}
				}
			}
		}
		
		//EXP Total Ganada en la Mazmorra (Integer)
		//int totalXP = (int) Math.round(XPGuanyada);
		
		double sumaXP = XPGuanyada + actualXP;
		
		int levelUp = 0;
		
		while (sumaXP >= metaXP) {
			
			sumaXP = sumaXP - metaXP;
			metaXP = metaXP + (metaXP * 0.2);
			
			
			nivel++;
			levelUp++;
		}
		
		//COMPRUEBA SI HA SUBIDO  +5 NIVELES
		
		String boss = "";
		
		if (levelUp >= 5) {
			boss = "ESTAS LISTO PARA LUCHAR CONTRA EL BOSS";
		} else {
			boss = "NO ESTAS PREPARADO PARA LUCHAR CONTRA EL BOSS";
		}
		
		String resultat = "";
		
		resultat = "HAS SUBIDO "+levelUp+" NIVELES\nEXP ACTUAL: " + (int)sumaXP+"\nEXP SIGUIENTE NIVEL: "+(int)metaXP+"\nNIVEL: "+nivel+"\n"+boss;
		
		return resultat;
	}
	
	

}
