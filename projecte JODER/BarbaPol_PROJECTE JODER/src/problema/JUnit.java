package problema;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class JUnit {
	
	te_falta_experiencia joder = new te_falta_experiencia();
	
	int [][] matriu1 = {{9, 0, 4, 7, 0},
					{0, 2, 0, 0, 1},
					{0, 0, 3, 3, 0},
					{5, 8, 0, 9, 1},
					{0, 0, 2, 0, 0}};
	
	int [][] matriu2 = {{0, 3, 2, 7, 2},
						{0, 2, 0, 0, 2},
						{0, 9, 2, 3, 0},
						{1, 0, 2, 0, 3},
						{5, 0, 0, 1, 2}};
	
	int [][] matriu3 = {{50, 50, 50, 50, 50},
						{50, 50, 50, 50, 50},
						{50, 50, 50, 50, 50},
						{50, 50, 50, 50, 50},
						{50, 50, 50, 50, 50}};
	
	
	@Test
	void testFacil() {
		assertEquals("HAS SUBIDO 2 NIVELES\nEXP ACTUAL: 437\nEXP SIGUIENTE NIVEL: 1078\nNIVEL: 7\nNO ESTAS PREPARADO PARA LUCHAR CONTRA EL BOSS", joder.resExperiencia(5, 285, 749, 30, matriu1));
		
		assertEquals("HAS SUBIDO 0 NIVELES\nEXP ACTUAL: 10764\nEXP SIGUIENTE NIVEL: 12835\nNIVEL: 99\nNO ESTAS PREPARADO PARA LUCHAR CONTRA EL BOSS", joder.resExperiencia(99, 8239, 12835, 50, matriu2));
		
		assertEquals("HAS SUBIDO 40 NIVELES\nEXP ACTUAL: 14066\nEXP SIGUIENTE NIVEL: 14697\nNIVEL: 885\nESTAS LISTO PARA LUCHAR CONTRA EL BOSS", joder.resExperiencia(845, 5, 10, 70, matriu3));
		
	}
	
	int [][] matriu4 = {{999, 999, 999, 999, 999},
			{999, 999, 999, 999, 999},
			{999, 999, 999, 999, 999},
			{999, 999, 999, 999, 999},
			{999, 999, 999, 999, 999}};
	
	int [][] matriu5 = {{4, 0, 2, 9, 1},
						{6, 7, 2, 0, 3},
						{0, 0, 2, 0, 5},
						{7, 3, 9, 0, 8},
						{7, 2, 3, 1, 4}};
	
	int [][] matriu6 = {{0, 0, 0, 0, 0},
						{0, 0, 0, 0, 0},
						{0, 0, 0, 0, 0},
						{0, 0, 0, 0, 0},
						{0, 0, 0, 0, 0}};
	
	
	@Test
	void testDificil() {
		assertEquals("HAS SUBIDO 0 NIVELES\nEXP ACTUAL: 9999\nEXP SIGUIENTE NIVEL: 99999\nNIVEL: 99999\nNO ESTAS PREPARADO PARA LUCHAR CONTRA EL BOSS", joder.resExperiencia(99999, 9999, 99999, 0, matriu4));
	
		assertEquals("HAS SUBIDO 0 NIVELES\nEXP ACTUAL: 937\nEXP SIGUIENTE NIVEL: 9328\nNIVEL: 87\nNO ESTAS PREPARADO PARA LUCHAR CONTRA EL BOSS", joder.resExperiencia(87, 5, 9328, 10, matriu5));
		
		assertEquals("HAS SUBIDO 0 NIVELES\nEXP ACTUAL: 499\nEXP SIGUIENTE NIVEL: 500\nNIVEL: 55\nNO ESTAS PREPARADO PARA LUCHAR CONTRA EL BOSS", joder.resExperiencia(55, 499, 500, 70, matriu6));
	}

}
