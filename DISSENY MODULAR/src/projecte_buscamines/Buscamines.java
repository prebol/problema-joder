package projecte_buscamines;

import java.util.Random;
import java.util.Scanner;

import Core.Board;
import Core.Window;

public class Buscamines {
	
	static Scanner sc = new Scanner(System.in);
	
	static Random r = new Random();

	static Board b = new Board();
	
	static Window w = new Window(b);
	
	static Usuari user = new Usuari();
	
	private static void inicialitzarGUI() {

		b.setColorbackground(0xb1adad); //posa el color de fons del background
		//el color estÃ  escrit en hexa RBG, Ã©s a dir, Ã©s un nombre hexadecimal que cada parella de 2 bits, de 00 a FF, representa el nombre RBG de 0 a 255. Per tant, 0xFF000 Ã©s vermell, 0x00FF00 Ã©s verd, i 0x0000FF Ã©s blau
		b.setActborder(true);  //fa que siguin visibles les vores entre caselles
		String[] lletres = { "      ", "      1", "      2", "      3", "      4", "      5", "      6", "      7", "      8", "      *", "      Ó" };  //quÃ¨ s'ha d'escriure en cada casella en base al nombre. Per tant, per exemple, si el nombre de la matriu Ã©s 0 no sâ€™escriurÃ  res, si Ã©s 1 sâ€™escriurÃ  la string â€œ1â€�, i si es 9 sâ€™escriurÃ  la String â€œ*â€�
		b.setText(lletres);
		int[] colorlletres = { 0x0000FF, 0x00FF00, 0xFFFF00, 0xFF0000, 0xFF00FF, 0x00FFFF, 0x521b98, 0xFFFFFF, 0xFF8000, 0x7F00FF, 0x000000 }; //el color de les lletres, que veient com funcionava la String de lletres ja entendreu com va
		b.setColortext(colorlletres);
		
		w.setActLabels(true);
		String[] etiquetes2 = {"Jugador: "+user.nom, "Num. Mines: "+user.numMines};  //les etiquetes que es mostraran a la dreta de la pantalla. Haureu de canviar el camp de mines per la vostra variable
		w.setLabels(etiquetes2);
		
		w.setTitle("Cercamines");
		
	}


	
	public static void main(String[] args) {
		
		
		
		user.nom = "Jugador1";
		
		user.filMapa = 8;
		user.colMapa = 8;
		
		user.numMines = 10;
		
		Boolean sortir = false;
		
		while (!sortir) {
			System.out.println("> MENU PRINCIPAL:");
			System.out.println("1) Mostrar ajuda");
			System.out.println("2) Opcions");
			System.out.println("3) Jugar partida");
			System.out.println("4) Veure rankings");
			System.out.println("0) Sortir");
			System.out.print("-> ");
			int opcio = sc.nextInt();
			switch (opcio) {
			case 0:
				sortir = true;
				System.out.println("\nGRACIES PER JUGAR!!!");
				break;
			case 1:
				mostrarAjuda();
				break;
			case 2:
				configurarOpcions(user);
				break;
			case 3:
				jugar(user);
				break;
			case 4:
				veureRankings(user);
				break;
			default:
				System.out.println("X Entrada Incorrecta X\n");
				break;
			}
		}
	}
	
	/**
	 * Imprimeix per pantalla tots els jugadors que han guanyat una partida en la sessiÃ³ actual
	 * @param user Classe que contÃ© la informaciÃ³ que necessita el usuari per jugar i personalitzar cada partida
	 */

	private static void veureRankings(Usuari user) {

		System.out.println("> RANKING:");
		System.out.println("-Aquests son els guanyadors:\n"+user.guanyadors);
		System.out.println("\nPulsa la tecla \"Enter\" per continuar...");
		sc.nextLine();
		sc.nextLine();
		
	}
	
	/**
	 * Inicia una partida nova del Buscamines
	 * @param user Classe que contÃ© la informaciÃ³ que necessita el usuari per jugar i personalitzar cada partida
	 */

	private static void jugar(Usuari user) {
		
		inicialitzarGUI();
		
		user.perdre = false;
		
		int [][] taulerJoc = inicialitzarCamp(user);

		int [][] taulerMines = inicialitzarMines(user);
		
		imprimirMatriu(user, taulerJoc);
		
		Boolean partidaEnCurs = true;
		
		while (partidaEnCurs) {
			//imprimirMatriu(user, taulerMines);
			demanarCoordenades(user);
			taulerJoc = descobrirTauler(user, taulerJoc, taulerMines);
			if (user.perdre) {
				partidaEnCurs = false;
				imprimirMatriu(user, taulerJoc);
				System.out.println("\nBOOM!!! HAS PERDUT :(\n");
			} else {
				taulerJoc = proximitatMines(user.posFil-1,user.posCol-1, user.filMapa, user.colMapa, taulerMines, taulerJoc);
				imprimirMatriu(user, taulerJoc);
				//imprimirMatriu(user, taulerMines);
				//System.out.println(contMines);
				Boolean victoria = comprovarVictoria(user, taulerJoc);
				if (victoria) {
					System.out.println("ENHORABONA, HAS GUANYAT AL BUSCAMINAS!!! ;D\n");
					user.guanyadors.add(user.nom);
					partidaEnCurs = false;
				} else {
					System.out.println("T'HAS SALVAT!!!\n");
				}
				
			}
			
			
		}
		
		
		
		//imprimirMatriu(user, taulerMines);
		
		
		
	}
	
	/**
	 * Comprova si en el tauler nomÃ©s estÃ¡n tapades aquelles caselles que contenen una bomba
	 * @param user Classe que contÃ© la informaciÃ³ que necessita el usuari per jugar i personalitzar cada partida
	 * @param taulerJoc El tauler on s'estÃ  jugant la partida actual
	 * @return "True" SI nomÃ©s estÃ n tapades les mines / "False" si encara quedan caselles tapades que NO sÃ³n mines
	 */
	
	private static Boolean comprovarVictoria(Usuari user, int [][] taulerJoc) {
		
		Boolean victoria = false;
		
		int cont9 = 0;
		
		for (int i = 0;i < user.filMapa;i++) {
			for (int j = 0;j < user.colMapa;j++) {
				if (taulerJoc[i][j] == 9) {
					cont9++;
				}
			}
		}
		
		if (cont9 == user.numMines) {
			victoria = true;
		}
		
		return victoria;
	}
	
	/**
	 * Comprova si hi han mines al voltant de la casella seleccionada per el jugador
	 * @param posFil Fila de la matriu seleccionada per el jugador
	 * @param posCol Columna de la matriu seleccionada per el jugador
	 * @param filMapa Quantitat de files del mapa/tauler
	 * @param colMapa Quantitat de columnes del mapa/tauler
	 * @param taulerMines Tauler amb la posiciÃ³ de totes les mines de la partida
	 * @param taulerJoc Tauler que veu el jugador, amb el qual interacciona
	 * @return Retorna una matriu amb les modificacions pertanyents depenen de quants espais lliures i mines hi han al voltant de la casella seleccionada
	 */

	private static int[][] proximitatMines(int posFil, int posCol, int filMapa, int colMapa, int[][] taulerMines, int[][] taulerJoc) {

		
			int contMines = 0;
			
			//System.out.println("proximitat fila "+user.posFil);
			//System.out.println("proximitat columna "+user.posCol);
			
			//ARRIBA
			if (!fuera(posFil-1, posCol, filMapa, colMapa)) {
				if (taulerMines[posFil-1][posCol] == 1) {
					contMines++;
				}
			}
			/*System.out.println((user.posFil-1) + " " + (user.posCol));
			System.out.println(contMines);*/
			
			//ARRIBA-IZQUIERDA
			if (!fuera(posFil-1, posCol-1, filMapa, colMapa)) {
				if (taulerMines[posFil-1][posCol-1] == 1) {
					contMines++;
				}
			}
			/*System.out.println((user.posFil-1) + " " + (user.posCol-1));
			System.out.println(contMines);*/
			
			//ARRIBA-DERECHA
			if (!fuera(posFil-1, posCol+1, filMapa, colMapa)) {
				if (taulerMines[posFil-1][posCol+1] == 1) {
					contMines++;
				}
			}
			/*System.out.println((user.posFil-1) + " " + (user.posCol+1));
			System.out.println(contMines);*/
			
			//IZQUIERDA
			if (!fuera(posFil, posCol-1, filMapa, colMapa)) {
				if (taulerMines[posFil][posCol-1] == 1) {
					contMines++;
				}
			}
			/*System.out.println((user.posFil) + " " + (user.posCol-1));
			System.out.println(contMines);*/
			
			//DERECHA
			if (!fuera(posFil, posCol+1, filMapa, colMapa)) {
				if (taulerMines[posFil][posCol+1] == 1) {
					contMines++;
				}
			}
			/*System.out.println((user.posFil) + " " + (user.posCol+1));
			System.out.println(contMines);*/
			
			//ABAJO
			if (!fuera(posFil+1, posCol, filMapa, colMapa)) {
				if (taulerMines[posFil+1][posCol] == 1) {
					contMines++;
				}
			}
			/*System.out.println((user.posFil+1) + " " + (user.posCol));
			System.out.println(contMines);*/
			
			//ABAJO-IZQUIERDA
			if (!fuera(posFil+1, posCol-1, filMapa, colMapa)) {
				if (taulerMines[posFil+1][posCol-1] == 1) {
					contMines++;
				}
			}
			/*System.out.println((user.posFil+1) + " " + (user.posCol-1));
			System.out.println(contMines);*/
			
			//ABAJO-DERECHA
			if (!fuera(posFil+1, posCol+1, filMapa, colMapa)) {
				if (taulerMines[posFil+1][posCol+1] == 1) {
					contMines++;
				}
			}
			//System.out.println((user.posFil+1) + " " + (user.posCol+1));
			//System.out.println(contMines);
			
			if (contMines == 0) {
				taulerJoc[posFil][posCol] = 0;
				
				if (!fuera(posFil-1, posCol, filMapa, colMapa) && (taulerJoc[posFil-1][posCol] == 9)) {
					proximitatMines(posFil-1,posCol, filMapa, colMapa, taulerMines, taulerJoc);
				}
				
				//ARRIBA-IZQUIERDA
				if (!fuera(posFil-1, posCol-1, filMapa, colMapa) && (taulerJoc[posFil-1][posCol-1] == 9)) {
					proximitatMines(posFil-1,posCol-1, filMapa, colMapa, taulerMines, taulerJoc);
				}
				
				//ARRIBA-DERECHA
				if (!fuera(posFil-1, posCol+1, filMapa, colMapa) && (taulerJoc[posFil-1][posCol+1] == 9)) {
					proximitatMines(posFil-1,posCol+1, filMapa, colMapa, taulerMines, taulerJoc);
				}
				
				//IZQUIERDA
				if (!fuera(posFil, posCol-1, filMapa, colMapa) && (taulerJoc[posFil][posCol-1] == 9)) {
					proximitatMines(posFil,posCol-1, filMapa, colMapa, taulerMines, taulerJoc);
				}
				
				//DERECHA
				if (!fuera(posFil, posCol+1, filMapa, colMapa) && (taulerJoc[posFil][posCol+1] == 9)) {
					proximitatMines(posFil,posCol+1, filMapa, colMapa, taulerMines, taulerJoc);
				}
				
				//ABAJO
				if (!fuera(posFil+1, posCol, filMapa, colMapa) && (taulerJoc[posFil+1][posCol] == 9)) {
					proximitatMines(posFil+1,posCol, filMapa, colMapa, taulerMines, taulerJoc);
				}
				
				//ABAJO-IZQUIERDA
				if (!fuera(posFil+1, posCol-1, filMapa, colMapa) && (taulerJoc[posFil+1][posCol-1] == 9)) {
					proximitatMines(posFil+1,posCol-1, filMapa, colMapa, taulerMines, taulerJoc);
				}
				
				//ABAJO-DERECHA
				if (!fuera(posFil+1, posCol+1, filMapa, colMapa) && (taulerJoc[posFil+1][posCol+1] == 9)) {
					proximitatMines(posFil+1,posCol+1, filMapa, colMapa, taulerMines, taulerJoc);
				}
			} else {
				taulerJoc[posFil][posCol] = contMines;
			}
		
		
		return taulerJoc;
		
	}
	
	/**
	 * Comprova si la posiciÃ³ donada estÃ  o no fora dels lÃ­mits indicats
	 * @param posFil Fila de la matriu seleccionada per el jugador
	 * @param posCol Columna de la matriu seleccionada per el jugador
	 * @param filMapa Quantitat de files del mapa/tauler
	 * @param colMapa Quantitat de columnes del mapa/tauler
	 * @return "True" si la posiciÃ³ SI estÃ  fora / "False" si la posiciÃ³ NO estÃ  fora
	 */

	private static Boolean fuera(int posFil, int posCol, int filMapa, int colMapa) {

		Boolean fuera = false;
		//System.out.println("fuera fila "+posFil);
		//System.out.println("fuera columna "+posCol);
		
		if ((posFil < 0 || posFil >= filMapa) || (posCol < 0 || posCol >= colMapa)) {
			fuera = true;
		}
		
		return fuera;
	}
	
	/**
	 * Destapa la posiciÃ³ indicada per el jugador
	 * @param user Classe que contÃ© la informaciÃ³ que necessita el usuari per jugar i personalitzar cada partida
	 * @param taulerJoc Tauler que veu el jugador, amb el qual interacciona
	 * @param taulerMines Tauler amb la posiciÃ³ de totes les mines de la partida
	 * @return Retorna el tauler de la partida amb la casella indicada destapada, i a mÃ©s a mÃ©s detecta si Ã©s una bomba, en aquest cas indica que s'ha perdut, sino, no fa rÃ©s mÃ©s
	 */

	private static int[][] descobrirTauler(Usuari user, int[][] taulerJoc, int[][] taulerMines) {
		
		taulerJoc[user.posFil-1][user.posCol-1] = taulerMines[user.posFil-1][user.posCol-1];
		
		if ((taulerJoc[user.posFil-1][user.posCol-1]) == 1) {
			(taulerJoc[user.posFil-1][user.posCol-1]) = 10;
			user.perdre = true;
		}
		
		return taulerJoc;
	}
	
	/**
	 * Demana al usuari/jugador quina casella vol seleccionar
	 * @param user Classe que contÃ© la informaciÃ³ que necessita el usuari per jugar i personalitzar cada partida
	 */

	private static void demanarCoordenades(Usuari user) {
		
		/*System.out.println("- Indica una posicio:");
		
		Boolean numFila = false;
		while (!numFila) {
			System.out.print("-> Fila: ");
			user.posFil = sc.nextInt();
			if (user.posFil > 0 && user.posFil <= user.filMapa) {
				numFila = true;
			} else {
				System.out.println("INCORRECTA");
			}
		}
		
		Boolean numCol = false;
		while (!numCol) {
			System.out.print("-> Columna: ");
			user.posCol = sc.nextInt();
			if (user.posCol > 0 && user.posCol <= user.colMapa) {
				numCol = true;
			} else {
				System.out.println("INCORRECTA");
			}
		}
		
		System.out.println();*/
		int fila = b.getMouseRow();
		int col = b.getMouseCol();
		
		while(b.getMouseRow() == fila && b.getMouseCol() == col) {
			System.out.println("CLICA SOBRE LA CASELLA");
		}
		
		user.posCol = b.getMouseCol()+1;
		System.out.println(user.posCol);
		user.posFil = b.getMouseRow()+1;
		System.out.println(user.posFil);
		
	}
	
	/**
	 * Aquesta funciÃ³ imprimeix una mariu de INT
	 * @param user Classe que contÃ© la informaciÃ³ que necessita el usuari per jugar i personalitzar cada partida
	 * @param tauler Matriu que Ã©s vol imprimir
	 */

	private static void imprimirMatriu(Usuari user, int[][] tauler) {
		
		b.draw(tauler, 't'); //la t implica que Ã©s mode text.


		System.out.println("> BUSCAMINAS:\n");
		
		for (int i = 0;i < user.filMapa;i++) {
			for (int j = 0;j < user.colMapa;j++) {
				System.out.print(tauler[i][j] + " ");
			}System.out.println();
		}
		System.out.println();
		
	}
	
	/**
	 * Aquesta variable crea un tauler omplert nomÃ©s amb 9's
	 * @param user Classe que contÃ© la informaciÃ³ que necessita el usuari per jugar i personalitzar cada partida
	 * @return Torna una matriu omplerta tot amb 9's
	 */

	private static int[][] inicialitzarCamp(Usuari user) {

		int [][] tauler = new int[user.filMapa][user.colMapa];
		
		for (int i = 0;i < user.filMapa;i++) {
			for (int j = 0;j < user.colMapa;j++) {
				tauler[i][j] = 9;
			}
		}
		
		return tauler;
	}
	
	/**
	 * Crear el tauler amb x quantitat de mines i la resta buit
	 * @param user Classe que contÃ© la informaciÃ³ que necessita el usuari per jugar i personalitzar cada partida
	 * @return Torna una matriu omplerta de 0's, i tambÃ© amb x nombre de 1's (mines) depenen de quantes s'han indicat posar
	 */

	private static int[][] inicialitzarMines(Usuari user) {
		
		int [][] tauler = new int[user.filMapa][user.colMapa];
		
		int minas = user.numMines;
		int posFila = user.filMapa;
		int posColumna = user.colMapa;
		
		for (int i = 0;i < minas;i++) {
			int fila = r.nextInt(posFila);
			//System.out.println(fila);
			int columna = r.nextInt(posColumna);
			//System.out.println(columna);
			
			if (tauler[fila][columna] == 1) {
				i--;
			} else {
				tauler[fila][columna] = 1;
			}
			
		}
		
		return tauler;
		
	}
	
	/**
	 * Serveix per canviar les configuracions que afectan a la jugabilitat del usuari (Nom, Num, de Mines, Tamany del Tauler & Num. de Mines)
	 * @param user Classe que contÃ© la informaciÃ³ que necessita el usuari per jugar i personalitzar cada partida
	 */

	private static void configurarOpcions(Usuari user) {

		Boolean sortir = false;
		
		while (!sortir) {
			System.out.println("\n> OPCIONS:");
			System.out.println("1) Canviar nom (Nom Actual: "+user.nom+")");
			System.out.println("2) Tamany del tauler (Tamany Actual: "+user.filMapa+"x"+user.colMapa+")");
			System.out.println("3) Numero de Mines (Mines Actuals: "+user.numMines+")");
			System.out.println("0) Sortir");
			System.out.print("-> ");
			int opcio = sc.nextInt();
			switch (opcio) {
			case 0:
				sortir = true;
				break;
			case 1:
				sc.nextLine();
				System.out.print("\n> Quin nom vols assignar al Jugador?:");
				System.out.print("-> ");
				user.nom = sc.nextLine();
				System.out.println();
				break;
			case 2:
				System.out.println("\n> Indica el tamany del nou tauler:");
				System.out.print("-> Filas: ");
				user.filMapa = sc.nextInt();
				//System.out.println();
				System.out.print("-> Columnas: ");
				user.colMapa = sc.nextInt();
				System.out.println();
				break;
			case 3:
				System.out.println("\nIndica el numero de mines que contindra el tauler:");
				System.out.print("-> ");
				user.numMines = sc.nextInt();
				System.out.println();
				break;
			default:
				System.out.println("\nX Entrada Incorrecta X\n");
				break;
			}
		}
		
	}
	
	/**
	 * Mostra ajuda de com funciona el joc
	 */

	private static void mostrarAjuda() {

		System.out.println("\n> INFO SOBRE EL JOC:");
		System.out.println("- Participa un jugador, descobrint una casella cada torn.");
		System.out.println("- La casella si no conte una mina indicaraï¿½ quantes mines estan en les 8 caselles adjacents.");
		System.out.println("- El joc acaba quan les uniques caselles sense descobrir siguin les mines, o be quan el jugador descobreix una mina.\n");
		System.out.println("Pulsa la tecla \"Enter\" per continuar...");
		sc.nextLine();
		sc.nextLine();
	}
	
}
